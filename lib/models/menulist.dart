import 'package:flutter/material.dart';

class MenuList {
  final List<MenuItem>? menu;
  MenuList({
    this.menu,
  });

  factory MenuList.fromJson(List<dynamic> parsedJson) {
    List<MenuItem> menu;
    menu = parsedJson.map((i) => MenuItem.fromJson(i)).toList();

    return MenuList(menu: menu);
  }
}

class MenuItem {
  final String title;
  final String? route;
  final IconData? icon;
  final List<MenuItem> children;

  MenuItem({
    required this.title,
    this.route,
    this.icon,
    this.children = const [],
  });

  factory MenuItem.fromJson(Map<String, dynamic> json) {
    return new MenuItem(
      title: json['title'],
      icon: IconData(int.parse(json['icon']), fontFamily: 'MaterialIcons'),
      route: json['route'],
      children: json['children'],
    );
  }
}
