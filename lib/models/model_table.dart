class TableModel {
  final String? name;
  List<ColumnModel>? column;
  final dynamic data;
  TableModel({
    this.name,
    this.column,
    this.data,
  });
}

class ColumnModel {
  final String? iconHeader;
  final String? field;
  final String? lavel;
  final String? widgetHeader;
  final String? iconCell;
  final String? widgetCell;
  ColumnModel({
    this.iconHeader,
    this.field,
    this.lavel,
    this.widgetCell,
    this.widgetHeader,
    this.iconCell,
  });
}
