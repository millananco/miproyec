import 'package:flutter/material.dart';

class MenuItem {
  const MenuItem({
    required this.title,
    this.route,
    this.icon,
    this.children = const [],
  });

  final String title;
  final String? route;
  final IconData? icon;
  final List<MenuItem> children;

  MenuItem.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        icon = json['icon'],
        route = json['route'],
        children = json['children'];

  Map<String, dynamic> toJson() => {
        'title': title,
        'icon': icon,
        'route': route,
        'children': children,
      };
}

final List<MenuItem> adminMenuItems = const [
  MenuItem(
    title: 'User Profile',
    icon: Icons.account_circle,
    route: '/',
  ),
  MenuItem(
    title: 'Settings',
    icon: Icons.settings,
    route: '/',
  ),
  MenuItem(
    title: 'Logout',
    icon: Icons.logout,
    route: '/',
  ),
];
