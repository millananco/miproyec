import 'package:miproyec/controllers/MenuController.dart';
import 'package:miproyec/responsive.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:miproyec/screens/dashboard/components/menupopup.dart';
import '../../../constants.dart';

import 'package:miproyec/models/menulist.dart';
//import 'menupopup.dart';
import 'dart:convert';

class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (!Responsive.isDesktop(context))
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: context.read<MenuController>().controlMenu,
          ),
        if (!Responsive.isMobile(context))
          Text(
            "Dashboard",
            style: Theme.of(context).textTheme.headline6,
          ),
        if (!Responsive.isMobile(context)) Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
        Expanded(child: SearchField()),
        ProfileCard()
      ],
    );
  }
}

class ProfileCard extends StatelessWidget {
  const ProfileCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String myjsonData = '[{ "title" : "User Profile", "icon" :"0xe043","route":"/" },{ "title" : "Settings", "icon" :"0xe57f","route":"/" },{ "title" : "Logout", "icon" : "0xe3b3","route":"/" }]';
    // String myjsonData = '[{ "title" : "User Profile", "icon" : "Icons.account_circle","route":"/" },{ "title" : "Settings", "icon" :"Icons.settings","route":"/" },{ "title" : "Logout", "icon" : "Icons.logout","route":"/" }]';

    final jsonResponse = json.decode(myjsonData);
    MenuList menuList = MenuList.fromJson(jsonResponse);
    /* 
    print("Menu " + menuList.menu![0].title);
    */
    return Container(
      margin: EdgeInsets.only(left: defaultPadding),
      padding: EdgeInsets.symmetric(
        horizontal: defaultPadding,
        vertical: defaultPadding / 2,
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.white10),
      ),
      child: Row(
        children: [
          Icon(Icons.account_circle, size: 30),
          /*  Image.asset(
           "assets/images/user_logo.png",
            height: 38,
          ),*/
          if (!Responsive.isMobile(context))
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
              child: Text("Andronio"),
            ),
          MenuPopup(menuItems: menuList),
          //  MenuPopup(menuItems: adminMenuItems),
          //Icon(Icons.keyboard_arrow_down),
        ],
      ),
    );
  }
}

class SearchField extends StatelessWidget {
  const SearchField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: "Search",
        fillColor: Theme.of(context).canvasColor,
        filled: true,
        border: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: const BorderRadius.all(Radius.circular(10)),
        ),
        suffixIcon: InkWell(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.all(defaultPadding * 0.75),
            margin: EdgeInsets.symmetric(horizontal: defaultPadding / 2),
            decoration: BoxDecoration(
              color: primaryColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: SvgPicture.asset("assets/icons/Search.svg"),
          ),
        ),
      ),
    );
  }
}
/*
final List<MenuItem> adminMenuItems = const [
  MenuItem(
    title: 'User Profile',
    icon: Icons.account_circle,
    route: '/',
  ),
  MenuItem(
    title: 'Settings',
    icon: Icons.settings,
    route: '/',
  ),
  MenuItem(
    title: 'Logout',
    icon: Icons.logout,
    route: '/',
  ),
];
*/
