import 'package:miproyec/models/model_table.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_svg/svg.dart';

import '../../../constants.dart';

class DataTable extends StatelessWidget {
  const DataTable({
    Key? key,
    this.datatable,
  }) : super(key: key);

  final TableModel? datatable;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(defaultPadding),
      decoration: BoxDecoration(
        color: Theme.of(context).canvasColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            datatable!.name!,
            style: Theme.of(context).textTheme.subtitle1,
          ),
          SizedBox(
            width: double.infinity,
            child: DataTable2(columnSpacing: defaultPadding, minWidth: 600, columns: datacolumn!, rows: datarow!),
          ),
        ],
      ),
    );
  }
}

List<DataCell>? datacell;
List<DataColumn>? datacolumn;
List<DataRow>? datarow;
