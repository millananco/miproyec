import 'package:flutter/material.dart';
import 'package:miproyec/models/menu.dart';

class MenuPopup extends StatelessWidget {
  const MenuPopup({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<MenuItem>(
      child: const Icon(Icons.keyboard_arrow_down), //Icons.account_circle
      itemBuilder: (context) {
        return adminMenuItems.map((MenuItem item) {
          return PopupMenuItem<MenuItem>(
            value: item,
            child: Row(
              children: [
                Icon(item.icon),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    item.title,
                    style: const TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          );
        }).toList();
      },
      onSelected: (item) {
        print('actions: onSelected(): title = ${item.title}, route = ${item.route}');
        Navigator.of(context).pushNamed(item.route!);
      },
    );
  }
}

final List<MenuItem> adminMenuItems = const [
  MenuItem(
    title: 'User Profile',
    icon: Icons.account_circle,
    route: '/',
  ),
  MenuItem(
    title: 'Settings',
    icon: Icons.settings,
    route: '/',
  ),
  MenuItem(
    title: 'Logout',
    icon: Icons.logout,
    route: '/',
  ),
];
