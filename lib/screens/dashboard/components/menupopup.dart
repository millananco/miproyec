import 'package:flutter/material.dart';
import 'package:miproyec/models/menulist.dart';

class MenuPopup extends StatelessWidget {
  const MenuPopup({Key? key, required this.menuItems}) : super(key: key);
  // final List<MenuItem> menuItems;
  final MenuList menuItems;
  @override
  Widget build(BuildContext context) {
    print('listmenu = ${menuItems.menu![0].title}');
    return PopupMenuButton<MenuItem>(
      child: Row(children: [
        Text("MySubMenu"),
        Icon(Icons.keyboard_arrow_down),
      ]), //Icons.account_circle
      itemBuilder: (context) {
        return menuItems.menu!.map((MenuItem item) {
          return PopupMenuItem<MenuItem>(
            value: item,
            child: Row(
              children: [
                Icon(item.icon),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    item.title,
                    style: const TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
              ],
            ),
          );
        }).toList();
      },
      onSelected: (item) {
        print('actions: onSelected(): title = ${item.title}, route = ${item.route}');
        Navigator.of(context).pushNamed(item.route!);
      },
    );
  }
}
/*
class MenuItem {
  const MenuItem({
    required this.title,
    this.route,
    this.icon,
    this.children = const [],
  });

  final String title;
  final String? route;
  final IconData? icon;
  final List<MenuItem> children;
}*/
/*Ejemplo de list menuitem
final List<MenuItem> adminMenuItems = const [
  MenuItem(
    title: 'User Profile',
    icon: Icons.account_circle,
    route: '/',
  ),
  MenuItem(
    title: 'Settings',
    icon: Icons.settings,
    route: '/',
  ),
  MenuItem(
    title: 'Logout',
    icon: Icons.logout,
    route: '/',
  ),
];

*/
