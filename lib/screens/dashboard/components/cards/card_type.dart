import 'package:miproyec/models/model_card.dart';
import 'package:flutter/material.dart';
import 'card_info.dart';
import 'card_maps.dart';

class CardType extends StatelessWidget {
  const CardType({
    Key? key,
    required this.info,
  }) : super(key: key);

  final CardModel info;

  @override
  Widget build(BuildContext context) {
    if (info.tipe == "cardinfo") {
      return CardInfo(info: info);
    } else if (info.tipe == "cardgps") {
      return CardMaps();
    } else {
      return CardInfo(info: info);
    }
  }
}
