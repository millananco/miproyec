import 'package:flutter/material.dart';
import 'package:miproyec/models/model_card.dart';
import '../../../../constants.dart';
import 'card_type.dart';

class CardGridView extends StatelessWidget {
  const CardGridView({
    Key? key,
    this.crossAxisCount = 4,
    this.childAspectRatio = 1,
  }) : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: demoCard.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: crossAxisCount,
        crossAxisSpacing: defaultPadding,
        mainAxisSpacing: defaultPadding,
        childAspectRatio: childAspectRatio,
      ),
      // itemBuilder: (context, index) => FileInfoCard(info: demoMyFiles[index]),
      itemBuilder: (context, index) => CardType(info: demoCard[index]),
    );
  }
}
