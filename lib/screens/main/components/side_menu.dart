import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:miproyec/responsive.dart';
import 'package:miproyec/screens/dashboard/components/menupopup.dart';
import 'package:miproyec/models/menulist.dart';
import 'dart:convert';

class SideMenu extends StatelessWidget {
  const SideMenu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String myjsonData = '[{ "title" : "User Profile", "icon" :"0xe043","route":"/" },{ "title" : "Settings", "icon" :"0xe57f","route":"/" },{ "title" : "Logout", "icon" : "0xe3b3","route":"/" }]';
    // String myjsonData = '[{ "title" : "User Profile", "icon" : "Icons.account_circle","route":"/" },{ "title" : "Settings", "icon" :"Icons.settings","route":"/" },{ "title" : "Logout", "icon" : "Icons.logout","route":"/" }]';

    final jsonResponse = json.decode(myjsonData);
    MenuList menuList = MenuList.fromJson(jsonResponse);
    List<Widget> myListMenu = [
      DrawerHeader(
        //   child: Image.asset("assets/images/Menu.jpg"),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/Menu.jpg'),
            // fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black26, BlendMode.darken),
            // image: NetworkImage('http://pic.qqtn.com/up/2016-4/2016042917263628852.jpg'),
            //  alignment: Alignment.topCenter,
            fit: BoxFit.fill,
          ),
          // color: Colors.blue,
        ),
        child: Text(
          'Distribucion',
          style: Theme.of(context).textTheme.headline6,
        ),
      ),
    ];
    myListMenu.add(DrawerListTile(
      title: "Dashboard",
      svgSrc: "assets/icons/menu_dashbord.svg",
      press: () {
        //   Navigator.pop(context);
      },
    ));
    myListMenu.add(DrawerListTile(
      title: "Valentina",
      svgSrc: "assets/icons/menu_task.svg",
      press: () {},
    ));
    myListMenu.add(DrawerListTile(
      title: "Settings",
      svgSrc: "assets/icons/menu_setting.svg",
      press: () {},
    ));
    myListMenu.add(DrawerListTile(
      title: "Notificacion",
      svgSrc: "assets/icons/menu_notification.svg",
      press: () {},
    ));
    myListMenu.add(MenuPopup(menuItems: menuList));
    return Drawer(
      child: ListView(children: myListMenu),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    // For selecting those three line once press "Command+D"
    required this.title,
    required this.svgSrc,
    required this.press,
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        press;
        if (!Responsive.isDesktop(context)) Navigator.pop(context);
      },
      horizontalTitleGap: 0.0,
      leading: SvgPicture.asset(
        svgSrc,
        // color: Colors.white70,
        color: Theme.of(context).primaryColor,
        height: 16,
      ),
      title: Text(
        title,
        style: Theme.of(context).textTheme.bodyText1,
      ),
    );
  }
}
